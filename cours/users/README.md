# Utilisateurs

> Woaw ça sonne pas bling-bling comme titre de cours ça. Vous me pardonnez j'essaie d'être concis et direct sur les cours écrits n_n

**Un cours sur la gestion d'utilisateurs, et pourquoi c'est le fondement de touuuuuuuuuuute la sécurité d'un OS, et donc la source de bonnes pratiques pour tous, développeurs comme admins.**

![boo](./img/boo.jpg)

## Sommaire

- [Utilisateurs](#utilisateurs)
  - [Sommaire](#sommaire)
  - [1. Utilisateurs et session](#1-utilisateurs-et-session)
  - [2. Identité des processus](#2-identité-des-processus)
  - [3. Le rapport avec rwx](#3-le-rapport-avec-rwx)
  - [4. Utilisateurs applicatifs](#4-utilisateurs-applicatifs)
  - [5. Pourquoi faut pas se connecter en root alors](#5-pourquoi-faut-pas-se-connecter-en-root-alors)
  - [6. Un mot sur sudo](#6-un-mot-sur-sudo)
  - [7. Bilan](#7-bilan)

## 1. Utilisateurs et session

Peu importe l'OS (Rocky Linux, Windows 11, MacOS, Android, etc.) il y a une gestion d'utilisateurs. Genre t'allumes la machine, et on te demande un user et un password.

Tu ouvres une session (y'a un *service* qui gère les sessions utilisateurs) sur la machine.

## 2. Identité des processus

> [Prérequis : voir le cours dédié sur les processus.](../processus/README.md)

➜ **Chaque programme exécuté depuis cette session est exécuté sous l'identité de cet utilisateur.**

C'est à dire que si ton utilisateur c'est "toto" et que tu lances un programme, en ouvrant le gestionnaire des tâches (ou avec un `ps -ef`) on verra que ce processus appartient à "toto".

💡💡💡 **C'est SUUUUUUPER IMPORTANT comme notion** parce que c'est ça qui détermine les privilèges du processus. Genre ses "droits", ses "privilèges", ses "permissions", c'est pareil tout ça, appelle ça comme tu veux.

➜ **Donc on peut dire qu'en fonction de qui lance le programme, il n'aura pas les mêmes droits sur la machine.**

J'sais pas moi par exemple, "toto" sur Windows, il a pas le droit de modifier des fichiers dans `C:/Windows/system32`, si t'essaies tu manges une erreur, et il te demande de le faire en tant que "administrateur". "administrateur" c'est un autre utilisateur qui a tous les droits sur la machine

Quand "administrateur" lance un truc, peu importe ce que c'est, ça ne lui sera jamais refusé (c'est l'utilisateur `root` sur Linux, MacOS et Android).

➜ **On se connecte jamais en tant que "administrateur"** (ni en tant que `root` donc), enfin on évite quoi, c'est une super mauvaise pratique si c'est pas strictement nécessaire.

**Genre lancer ton navigateur web en tant qu'admin c'est suicidaire.** Faut se dire que si tu visites un site louche qui essaie de te hack, bah il a les permissions admin sur ta machine du coup. BAH OUAIS. Donc fais pas ça. Et lance pas tes jeux en admin non plus.

## 3. Le rapport avec rwx

> [Prérequis : voir le cours dédié sur les permissions `rwx`](../permissions/README.md).

➜ Concrètement, c'est quoi ton OSde quoi il est constitué ?

On l'a déjà dit, l'OS c'est un ensemble de programmes. Un programme c'est un fichier exécutable.

Et toi tes jeux c'est des programmes, donc des fichiers. Et tes photos de famille et ta musique c'est des fichiers.

➜ **Bref un OS c'est des fichiers.** Et c'est tout. Rien d'autres.

Donc :

- d'un côté on a des processus qui s'exécute sous l'identité d'un utilisateur
- et de l'autre des fichiers, qui appartiennent à un utilisateur donné et munis de permissions `rwx`

💡💡💡 **DOOOONC choisir quel utilisateur lance un programme c'est choisir quels droits attribuer au processus**

Pour des raisons de sécurité, sur un serveur, on est super minutieux avec ça.

## 4. Utilisateurs applicatifs

➜ **C'est pour ça que dans `/etc/passwd` y'a pleiiiin d'utilisateurs présent dès l'installation**

Le délire c'est qu'on va créer un utilisateur dédié à l'exécution de un seul programme. Il a le droit de lire/écrire/exécuter juste certains fichiers, et rien d'autres.

Un nouveau *service* à lancer ? On crée un utilisateur dédié à l'exécution de juste ce *service* là.

> Genre il existe sur mon PC un utilisateur dédié à l'exécution du programme qui gère le bluetooth. Il sert qu'à ça. On peut même pas se connecter avec. Il sert juste à lancer le programme qui gère le bluetooth, sous forme de *service*.

💡💡💡 **Sur un serveur c'est normal, c'est courant, c'est crucial et c'est une bonne pratique, à chaque fois qu'on ajoute un service, on ajoute un utilisateur** qui sera dédié à l'exécution de ce service et qui n'aura aucun autre droit sur aucun autre fichier.

**On trouve parfois le terme "utilisateur applicatif"** pour désigner ces utilisateurs qui sont uniquement dédiés à lancer un service, et on est pas censés se connecter avec.

➜ **C'est de la sécurité élémentaire**

Sécurité dans le sens :

- ouais les vilains hackers, c'est un vrai sujet, mais pas que
- si un programme se met à bug/dysfonctionner, et par exemple, se met à essayer de supprimer tous les fichiers du disque dur, bah il pourra pas

## 5. Pourquoi faut pas se connecter en root alors

![i am root](./img/roooot.png)

Normalement si t'es arrivé jusque là et que t'as intégré les lignes précédentes, tu dois commencer à voir pourquoi se connecter en `root` (sous Linux ou MacOS ou Android) ou en "Administrateur" (sous Windows) c'est considéré comme une mauvaise pratique.

On préfère, sous Linux, en terminal, utiliser ponctuellement `sudo` quand c'est strictement nécessaire, sans se connecter directement en tant que `root`.

**Explicitons les raisons.**

➜ **C'est dangereux** pour un PC client

Sur un PC client, genre nos PC portables quoi, où on bosse, où on geek etc, c'est juste suicidaire de lancer une application comme un navigateur web ou un jeu en admin (ou `root`).

Là on parle clairement pour du hack, c'est un vecteur super exploité. Y'a plein de monde qui visitent des sites Web, plein de monde qui jouent à Minecraft, c'est la quantité, la masse, qui rend le truc intéressant pour un hacker malveillant.

> J'peux vous raconter des anecdotes à la pelle sur le sujet, qu'elles soient persos, ou proches, ou publiques. Lancez pas vos jeux en admin pitié :)

![Root access](./img/root_access.jpg)

➜ **C'est encore plus dangereux** pour un serveur

Non mais si t'as peur maintenant de te faire voler ton code de CB, est-ce que tu penses que tu peux te permettre en tant qu'hébergeur ou dév de site web de e-commerce de te permettre de leak le numéro de CB de tous tes clients ?

Donc ouais sur un serveur, c'est encore plus crucial, généralement :

- `root` on l'utilise pas directement
- `toto` : notre user, il a accès aux droits `root` avec `sudo` (ou Clic Droit > Exécuter en tant que Administrateur
- des utilisateurs applicatifs, un par programme/service

➜ **J'ai dit c'est dangereux**

Surtout en ligne de commande, c'est meurtrier. Tu dérapes avec ton petit doigt sur une commande pour supprimer une photo et tu supprimes tout ton disque dur. ***Perdu bro.***

Si t'utilises pas `root` y'a pas de soucis, ça ne peut pas fonctionner une telle erreur.

> Ca aussi je peux vous raconter des anecdotes dont j'suis pas fier :(

➜ **Ca fait prendre des mauvaises habitudes**

En utilisant tout le temps `root` on ne fait pas la distinction entre une opération privilégiée, qui nécessite vraiment les droits `root`, d'une opératio triviale qui nécessite rien de particulier.

Tu t'habitues vite à une conception biaisée de l'OS, et tu t'habitues à lancer tes programmes en `root` et c'est daaaaaangereux on a dit.

**Bref, tu t'habitues à faire de la merde.** Alors que ne pas utiliser `root` c'est un truc instructif en soit : tu as une meilleure compréhension des droits quand tu l'utilises pas, ou juste quand nécessaire avec `sudo`.

## 6. Un mot sur sudo

➜ **`sudo` est une commande Linux super pwatique pour exécuter un programme sous l'identité d'un autre utilisateur.**

> `sudo` c'est pour "Switch User DO". Genre "changer d'utilisateur et faire un truc". "faire un truc" dans un OS c'est lancer un programme hein.

On peut faire `sudo -u toto ls` pour exécuter `ls` en tant que `toto`.

Si on précise pas de user avec `-u`, c'est `root` par défaut (parce qu'on s'en sert principalement pour ça, donc ça évite d'être chiant et d'avoir besoin de le taper à chaque fois le `-u root`).

➜ **Il existe un fichier de conf `/etc/sudoers` qui détermine quel user a le droit de lancer quelle commande en tant que quel autre user**

Par défaut, sur un système Rocky Linux, il existe un groupe d'utilisateurs `wheel` dont les membres peuvent lancer n'importe quelle commande en tant que n'importe quel user (= "être admin")

Mais on peut faire des trucs fins comme  `toto` n'a le droit de lancer que la commande `ls` en tant que `lala`. Genre `toto` il peut saisir `sudo -u lala ls` mais tout autre commande `sudo` lui sera refusé.

![Make me a sandwich](./img/sandwich.png)

## 7. Bilan

En version courte ça donne :

➜ **L'OS propose une gestion d'utilisateurs**

- des users et des passwords
- gestion de de session

➜ **Chaque processus est exécuté sous l'identité d'un utilisateur précis**

- quand un programme est lancé, c'est un utilisateur spécifique qui le lance
- on peut voir avec `ps -ef` sous Linux quel processus appartient à quel user

➜ **Chaque fichier de la machine a des droits précis**

- il appartient à un user et à un groupe
- des permissions `rwx` qui déterminent qui a le droit de faire quoi

➜ **Choisir quel user exécute un programme c'est donc choisir les droits du programmes**

- il est nécessaire d'être minutieux quant au choix de l'utilisateur pour faire des tâches
- on utilise des *utilisateurs applicatifs* sur les serveurs : dédiés à n'exécuté qu'un seul programme
- on évite d'utiliser `root` ou "Administrateur" directement car c'est dangereux (on préfère utiliser `sudo` ponctuellement quand c'est nécessaire)
