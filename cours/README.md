# Cours

- [Intro OS et Linux](./intro/README.md)
- [Utilisateurs](./users/README.md)
- [Processus](./processus/README.md)
- [Permissions](./permissions/README.md)
- [Scripting](./scripting/README.md)
- [Shell](./shell/README.md)

## Mémos

- [Shell et commandes](./memo/shell.md)
- [Rocky network](./memo/rocky_network.md)
- [Installation de la VM Rocky](./memo/install_vm.md)
- [LVM](./memo/lvm.md)
