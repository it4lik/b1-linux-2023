# B1 Linux 2023

Ici vous trouverez tous les supports de cours, TPs et autres ressources liées au cours.

## [Cours](./cours/README.md)

- [Intro OS et Linux](./cours/intro/README.md)
- [Utilisateurs](./cours/users/README.md)
- [Processus](./cours/processus/README.md)
- [Permissions](./cours/permissions/README.md)
- [Scripting](./cours/scripting/README.md)
- [Shell](./cours/shell/README.md)

## [TPs](./tp/README.md)

- [TP1 : Casser avant de construire](./tp/1/README.md)
- [TP2 : Appréhension d'un système Linux](./tp/2/README.md)
- [TP3 : Services](./tp/3/README.md)
- [TP4 : Real services](./tp/4/README.md)
- [TP5 : We do a little scripting](./tp/5/README.md)
- [TP6 : Self-hosted cloud](./tp/6/README.md)

### [Mémo](./cours/memo/README.md)

- [Shell et commandes](./cours/memo/shell.md)
- [Rocky network](./cours/memo/rocky_network.md)
- [Installation de la VM Rocky](./cours/memo/install_vm.md)
- [LVM](./cours/memo/lvm.md)

![Be root](./img/beroot.jpg)
