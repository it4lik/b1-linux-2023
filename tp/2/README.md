# TP2 : Appréhension d'un système Linux

Ce TP a pour but de vous **faire manipuler quelques aspects élémentaires** d'un OS, et plus spécifiquement, d'un OS basé sur Linux.

Ce sera pas le TP dans lequel on fera les trucs les plus fous, mais il contient néanmoins du savoir essentiel pour pouvoir aller faire des choses plus complexes *(faut apprendre l'alphabet avant de lire Shakespeare)*.

![Wizard](./img/wizard.png)

## Sommaire

- [TP2 : Appréhension d'un système Linux](#tp2--appréhension-dun-système-linux)
  - [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
- [Suite du TP](#suite-du-tp)

# 0. Prérequis

➜ **VM prête à être clonée**

- vous pouvez utiliser l'OS de votre choix
- seule contrainte : ce doit être un OS avec une interface graphique
  - même si tout doit être fait en ligne de commande, ça vous permet d'utiliser un environnement un peu familier
  - et peut être de vous aider de l'interface graphique pour mieux comprendre certains points
- Ubuntu, Fedora, Kali, on s'en fout un peu pour ce TP ! Ce sera pareil partout !

➜ **Tout doit être fait depuis la ligne de commande**

- je vous recommande de vous connecter en SSH à la VM si vous êtes à l'aise avec ça
- sinon, on aura un cours dédié à ça rapidement de toute façon

➜ **Votre VM doit avoir un accès internet**

- il "suffit" d'ajouter une carte NAT à la VM depuis l'interface de VirtualBox
- aucune configuration requise pour une carte NAT, c'est censé fonctionner tout de suite
- pour tester que vous avez un accès internet, vous pouvez taper la commande `ping ynov.com` par exemple

➜ **Votre PC doit pouvoir joindre votre VM**

- avec VirtualBox, il faut ajouter une carte host-only à la VM
- deux cartes réseau donc : une host-only (pour pouvoir joindre la VM) et une NAT (pour avoir accès internet)

# Suite du TP

J'ai découpé en 3 parties pour que ce soit un peu plus cool à lire :

- [**Partie 1** : Files and users](./file_users.md)
- [**Partie 2** : Process & Packages](./processes_packages.md)
- [**Partie 3** : Poupée russe](./matryoshka.md)
