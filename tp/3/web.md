# II. Service HTTP

**Dans cette partie, on ne va pas se limiter à un service déjà présent sur la machine : on va ajouter un service à la machine.**

➜ **On va faire dans le *clasico* et installer un serveur HTTP très réputé : NGINX**

Un serveur HTTP permet d'héberger des sites web.

➜ **Un serveur HTTP (ou "serveur Web") c'est :**

- un programme qui écoute sur un port (ouais ça change pas ça)
- il permet d'héberger des sites web
  - un site web c'est un tas de pages html, js, css
  - un site web c'est aussi parfois du code php, go, python, ou autres, qui indiquent comment le site doit se comporter
- il permet à des clients de visiter les sites web hébergés
  - pour ça, il faut un client HTTP (par exemple, un navigateur web, ou la commande `curl`)
  - le client peut alors se connecter au port du serveur (connu à l'avance)
  - une fois le tunnel de communication établi, le client effectuera des *requêtes HTTP*
  - le serveur répondra par des *réponses HTTP*

> Une requête HTTP c'est "donne moi tel fichier HTML". Une réponse c'est "voici tel fichier HTML" + le fichier HTML en question.

**Ok bon on y va ?**

- [II. Service HTTP](#ii-service-http)
  - [1. Mise en place](#1-mise-en-place)
  - [2. Analyser la conf de NGINX](#2-analyser-la-conf-de-nginx)
  - [3. Déployer un nouveau site web](#3-déployer-un-nouveau-site-web)

## 1. Mise en place

![nngijgingingingijijnx ?](./img/njgjgijigngignx.jpg)

> Si jamais, pour la prononciation, NGINX ça vient de "engine-X" et vu que c'était naze comme nom... ils ont choisi un truc imprononçable ouais je sais cherchez pas la logique.

🌞 **Installer le serveur NGINX**

- il faut faire une commande `dnf install`
- pour trouver le paquet à installer :
  - `dnf search <VOTRE RECHERCHE>`
  - ou une recherche Google (mais si `dnf search` suffit, c'est useless de faire une recherche pour ça)

🌞 **Démarrer le service NGINX**

🌞 **Déterminer sur quel port tourne NGINX**

- vous devez filtrer la sortie de la commande utilisée pour n'afficher que les lignes demandées
- ouvrez le port concerné dans le firewall

> **NB : c'est bientôt la dernière fois que je signale explicitement la nécessité d'ouvrir un port dans le firewall.** Vous devrez vous-mêmes y penser lorsque nécessaire. C'est simple en vrai : dès qu'un truc écoute sur un port, faut ouvrir ce port dans le firewall. *Toutes les commandes liées au firewall doivent malgré tout figurer dans les compte-rendus.*

🌞 **Déterminer les processus liés au service NGINX**

- vous devez filtrer la sortie de la commande utilisée pour n'afficher que les lignes demandées

🌞 **Déterminer le nom de l'utilisateur qui lance NGINX**

- vous devriez le voir dans la commande `ps` précédente
- si l'utilisateur existe, alors il est listé dans le fichier `/etc/passwd`
  - je veux un `cat /etc/passwd | grep <USER>` pour mettre en évidence l'utilisateur qui lance NGINX

🌞 **Test !**

- visitez le site web
  - ouvrez votre navigateur (sur votre PC) et visitez `http://<IP_VM>:<PORT>`
  - vous pouvez aussi (toujours sur votre PC) utiliser la commande `curl` depuis un terminal pour faire une requête HTTP et visiter le site
- dans le compte-rendu, je veux le `curl` (pas un screen de navigateur)
  - utilisez Git Bash si vous êtes sous Windows (obligatoire parce que le `curl` de Powershell il fait des dingueries)
  - vous utiliserez `| head` après le `curl` pour afficher que les 7 premières lignes

## 2. Analyser la conf de NGINX

🌞 **Déterminer le path du fichier de configuration de NGINX**

- faites un `ls -al <PATH_VERS_LE_FICHIER>` pour le compte-rendu
- la conf c'est dans `/etc/` normalement, comme toujours !

🌞 **Trouver dans le fichier de conf**

- les lignes qui permettent de faire tourner un site web d'accueil (la page moche que vous avez vu avec votre navigateur)
  - ce que vous cherchez, c'est un bloc `server { }` dans le fichier de conf
  - vous ferez un `cat <FICHIER> | grep <TEXTE> -A X` pour me montrer les lignes concernées dans le compte-rendu
    - l'option `-A X` permet d'afficher aussi les `X` lignes après chaque ligne trouvée par `grep`
- une ligne qui commence par `include`
  - cette ligne permet d'inclure d'autres fichiers
  - bah ouais, on stocke pas toute la conf dans un seul fichier, sinon ça serait le bordel
  - encore un `cat <FICHIER> | grep <TEXTE>` pour ne montrer que cette ligne

## 3. Déployer un nouveau site web

🌞 **Créer un site web**

- bon on est pas en cours de design ici, alors on va faire simplissime
- créer un sous-dossier dans `/var/www/`
  - par convention, on stocke les sites web dans `/var/www/`
  - votre dossier doit porter le nom `tp3_linux`
- dans ce dossier `/var/www/tp3_linux`, créez un fichier `index.html`
  - il doit contenir `<h1>MEOW mon premier serveur web</h1>`

🌞 **Gérer les permissions**

- tout le contenu du dossier  `/var/www/tp3_linux` doit appartenir à l'utilisateur qui lance NGINX

🌞 **Adapter la conf NGINX**

- dans le fichier de conf principal
  - vous supprimerez le bloc `server {}` repéré plus tôt pour que NGINX ne serve plus le site par défaut
  - redémarrez NGINX pour que les changements prennent effet
- créez un nouveau fichier de conf
  - il doit être nommé correctement
  - il doit être placé dans le bon dossier
  - c'est quoi un "nom correct" et "le bon dossier" ?
    - bah vous avez repéré dans la partie d'avant les fichiers qui sont inclus par le fichier de conf principal non ?
    - créez votre fichier en conséquence
  - redémarrez NGINX pour que les changements prennent effet
  - le contenu doit être le suivant :

```nginx
server {
  # le port choisi devra être obtenu avec un 'echo $RANDOM' là encore
  listen <PORT>;

  root /var/www/tp3_linux;
}
```

🌞 **Visitez votre super site web**

- toujours avec une commande `curl` depuis votre PC (ou un navigateur)
