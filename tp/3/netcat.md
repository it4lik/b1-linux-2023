# III. Your own services

Dans cette partie, on va créer notre propre service :)

HE ! Vous vous souvenez de `netcat` ou `nc` ? Le ptit machin de notre premier cours de réseau ? C'EST L'HEURE DE LE RESORTIR DES PLACARDS.

- [III. Your own services](#iii-your-own-services)
  - [1. Au cas où vous l'auriez oublié](#1-au-cas-où-vous-lauriez-oublié)
  - [2. Analyse des services existants](#2-analyse-des-services-existants)
  - [3. Création de service](#3-création-de-service)

## 1. Au cas où vous l'auriez oublié

> Rien à écrire dans le rendu pour cette partie, c'juste pour vous remettre `nc` en main.

➜ Dans la VM

- `nc -l 8888`
  - lance netcat en mode listen
  - il écoute sur le port 8888
  - sans rien préciser de plus, c'est le port 8888 TCP qui est utilisé

➜ Connectez-vous au netcat qui est en écoute sur la VM

- depuis votre PC ou depuis une autre VM (il faut avoir la commande `nc`)
- `nc <IP_PREMIERE_VM> 8888`
- vérifiez que vous pouvez envoyer des messages dans les deux sens

> N'oubliez pas d'ouvrir le port 8888/tcp de la première VM bien sûr :)

## 2. Analyse des services existants

Un service c'est quoi concrètement ? C'est juste un processus, que le système lance, et dont il s'occupe après.

Il est défini dans un simple fichier texte, qui contient une info primordiale : quel programme doit être lancé quand le service "démarre".

> *"démarrer" un service c'est juste lancer un programme. Mais l'OS prend soin d'entretenir ce programme. Si on lance un programme à la main, l'OS il s'en balec un peu.*

Voyons un peu comment sont définis les services `sshd` et `nginx` avant de créer le nôtre.

🌞 **Afficher le fichier de service SSH**

- vous pouvez obtenir son chemin avec un `systemctl status <SERVICE>`
- mettez en évidence la ligne qui commence par `ExecStart=`
  - encore un `cat <FICHIER> | grep <TEXTE>`
  - c'est la ligne qui définit la commande lancée lorsqu'on "start" le service
  
> *Taper `systemctl start <SERVICE>` ou exécuter la commande indiquée après `ExecStart=` à la main, c'est (presque) pareil.*

🌞 **Afficher le fichier de service NGINX**

- mettez en évidence la ligne qui commence par `ExecStart=`

## 3. Création de service

![Create service](./img/create_service.png)

Bon ! On va créer un petit service qui lance un `nc`. Et vous allez tout de suite voir pourquoi c'est pratique d'en faire un service et pas juste le lancer à la main : on va faire en sorte que `nc` se relance tout seul quand un client se déconnecte.

> *Le comportement de base de `nc` c'est de quitter, de se fermer, si un utilisateur se connecte puis s'en va.*

Ca reste un truc pour s'exercer, c'pas non plus le truc le plus utile de l'année que de mettre un `nc` dans un service n_n

🌞 **Créez le fichier `/etc/systemd/system/tp3_nc.service`**

- vous remplacerez `<PORT>` par un numéro de port random obtenu avec la même méthode que précédemment
- son contenu doit être le suivant (nice & easy)

```service
[Unit]
Description=Super netcat tout fou

[Service]
ExecStart=/usr/bin/nc -l <PORT> -k
```

🌞 **Indiquer au système qu'on a modifié les fichiers de service**

- la commande c'est `sudo systemctl daemon-reload`

🌞 **Démarrer notre service de ouf**

- avec une commande `systemctl start`

🌞 **Vérifier que ça fonctionne**

- vérifier que le service tourne avec un `systemctl status <SERVICE>`
- vérifier que `nc` écoute bien derrière un port avec un `ss`
  - vous filtrerez avec un `| grep` la sortie de la commande pour n'afficher que les lignes intéressantes
- vérifer que juste ça fonctionne en vous connectant au service depuis une autre VM ou votre PC

> Si vous galérez avec votre PC c'est ptet que t'es sous Windows et qu'il chie pas mal dans la colle sur le réseau comme d'hab. Hésite pas à pop une deuxième VM pour faire des tests. Ou ptet utilise Git Bash.

🌞 **Les logs de votre service**

- mais euh, ça s'affiche où les messages envoyés par le client ? Dans les logs !
- `sudo journalctl -xe -u tp3_nc` pour visualiser les logs de votre service
- `sudo journalctl -xe -u tp3_nc -f` pour visualiser **en temps réel** les logs de votre service
  - `-f` comme follow (on "suit" l'arrivée des logs en temps réel)
- dans le compte-rendu je veux
  - une commande `journalctl` filtrée avec `grep` qui affiche la ligne qui indique le démarrage du service
  - une commande `journalctl` filtrée avec `grep` qui affiche un message reçu qui a été envoyé par le client
  - une commande `journalctl` filtrée avec `grep` qui affiche la ligne qui indique l'arrêt du service

🌞 **S'amuser à `kill` le processus**

- repérez le PID du processus `nc` lancé par votre *service*
- utilisez la commande `kill` pour mettre fin à ce processus `nc`

🌞 **Affiner la définition du service**

- faire en sorte que le service redémarre automatiquement s'il se termine
  - ajoutez `Restart=always` dans la section `[Service]` de votre service
  - n'oubliez pas d'indiquer au système que vous avez modifié les fichiers de service :)
- normalement, quand tu `kill` il est donc relancé automatiquement

