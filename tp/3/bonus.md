# Bonus

## 1. fail2ban

fail2ban est un outil qui permet de surveiller des fichiers texte, et opérer des actions quand il repère un motif particulier dans un fichier texte surveillé.

Concrètement, son utilisation principale : il surveille des fichiers de logs, et il exécute une commande pour ban l'IP d'un client qui a un comportement suspect.

Par exemple : plusieurs connexions successives échouées sur un serveur SSH, en très peu de temps, c'est sûrement un bruteforce : il faut ban l'IP source qui effectue ces tentatives de co.

> Mais on pourrait s'en servir pour déclencher une fusée s'il y'a écrit 4 fois chaussettes dans un fichier `chaussette.txt` si on [veut.](veut..md)

➜ Installer fail2ban

➜ Configurer un ban IP sur une surveillance des logs SSH

- si un client fail de se connecter 3 fois d'affilée en moins de 12 secondes
- on le ban IP dans le firewall de la machine

➜ Tester que ça fonctionne

- faites-vous ban
- utiliser une commande sur la VM pour lister les IPs qui ont été ban
- prouver que l'IP a été ban dans le firewall de la machine

➜ Affiner la conf pour supporter aussi la surveillance des logs NGINX

- si un client fait + de 20 requêtes HTTP en moins de 1 minutes
- on le ban IP dans le firewall de la machine
- idem, prouvez que le ban fonctionne

## 2. Wordpress

Wordpress est un *CMS* : un site web pré-développé, qui est relativement simple d'utilisation pour un non-initié. Une fois qu'il a été installé par un technicien, un non-initié peut se connetcer à l'interface Web et ajouter relativement facilement de nouvelles pages Web.

C'est dév en PHP, et il a deux trois prérequis pour fonctionner, c'est un peu un cas d'école !

➜ **Installer Wordpress sur la machine**

- c'est NGINX qui doit servir le site Wordpress (on a déjà installé NGINX)
  - pas de Apache quoi
- y'a 1000 docs en ligne pour faire ça
- il faudra aussi probablement ajouter une base de données à la machine...

➜ **Le but :**

- le site nul que vous aviez hébergé pendant le TP3 doit être dispo depuis votre PC à l'URL `http://site_nul.tp3.b1`
- le site Wordpress doit être dispo depuis votre PC à l'URL `http://wordpress.tp3.b1`

## 3. HTTPS

➜ **Permettre l'accès au(x) site(s) en HTTPS**

- utilisez un certificat auto-signé
- vous pouvez faire en sorte, malgré un certificat auto-signé, de proposer une connexion HTTPS avec un cadenas vert (complètement digne de confiance)
- venez vers moi si besoin de précisions à ce sujet !

