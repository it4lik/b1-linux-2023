## Checklist

> Habituez-vous à voir cette petite checklist, elle figurera dans tous les TPs.

A chaque machine déployée, vous **DEVREZ** vérifier la 📝**checklist**📝 :

- [x] IP locale, statique ou dynamique
- [x] hostname défini
- [x] SSH fonctionnel
- [x] accès Internet
- [x] SELinux en mode *"permissive"* vérifiez avec `sestatus`, voir [mémo install VM tout en bas](../../cours/memo/install_vm.md)

**Les éléments de la 📝checklist📝 sont STRICTEMENT OBLIGATOIRES à réaliser mais ne doivent PAS figurer dans le rendu.**

![Checklist](./img/checklist_is_here.jpg)

Pour cette première fois, petite explication de chacun des points, c'est qu'des trucs qu'on a déjà fait/vu normalement :

### IP locale, statique ou dynamique

Ca veut dire que t'as mis une interface *host-only* (*privé hôte* en français) à ta VM.

Ca veut aussi dire que ta VM a une adresse IP sur cette carte réseau (check [le mémo Rocky réseau](../../cours/memo/rocky_network.md) pour voir comment définir une IP à ta VM).

Et aussi que cette adresse IP est dans le même réseau que celle de ton PC.

> Genre si tu connectes ta VM au réseau *host-only* 1 ça veut dire que ton PC a aussi une carte réseau connecté à ce réseau *host-only* 1 et il a déjà une adresse IP dans ce réseau. Il faut que l'adresse IP de la VM soit dans le même réseau que celle du PC.

**Ca permet au PC et à la VM de communiquer. Aussi à plusieurs VMs de communiquer entre elles.**

> "communiquer" ça veut dire envoyer des `ping`, faire des connexion `ssh` ou jouer à CS en LAN.

### Hostname défini

Ca veut dire que t'as rempli le fichier `/etc/hostname` avec le nom de la machine (c'est un simple fichier texte, donc tu peux utiliser `nano`). Le fichier est lu au démarrage de la machine (par un *service* qui s'occupe de ça ;) ).

Genre t'écris juste le nom de la machine dans le fichier, rien d'autre.

---

En plus de remplir ce fichier, si tu veux que ce soit actif immédiatement sans avoir besoin de reboot, tu peux utiliser la commande `hostname` ([check le mémo de commandes](../../cours/memo/shell.md) pour voir comment t'en servir, tu fais CTRL + F "hostname" dans le doc).

Ca permet que la machine ait un nom, c'est clean, on fait ça systématiquement dans la vie réelle.

Aussi, tu auras le nom de la machine écrit dans ton prompt : `[it4@super_machine ~]$` pour mieux s'y retrouver.

### SSH fonctionnel

Ca veut dire que tu peux taper une commande `ssh@IP` pour te connecter à un terminal de la VM.

`IP` c'est l'adresse IP de la carte réseau *host-only* de la VM.

Ca permet d'avoir facilement plusieurs terminaux sur plusieurs VMs, et d'utiliser une interface cool (mais siiii c'est cool le terminal, t'es juste pas assez habitué encore).

On utilise ça tout le temps dans la vie réelle, car un serveur est généralement très loin physiquement de là où on se trouve, donc SSH permet de prendre le contrôle à distance.

> C'est juste du terminal, parce qu'on a pas besoin d'interface graphique pour administrer un serveur Linux, tout est faisable en ligne de commande bien plus facilement.

### Accès internet

Ca veut dire que t'as mis une carte NAT à ta VM (en + de la carte host-only).

Normalement y'a pas besoin de la conf, elle récup une IP automatiquement et t'as internet.

Tu peux faire un `ping ynov.com` pour vérifier que t'as internet.

### SELinux en mode permissive

Ca c'est que pour Rocky Linux. En bref parce que c'est pas le sujet : SELinux c'est un programme pour améliorer la sécurité des OS Linux. C'est cool mais c'est chiant à conf et c'est pas du tout l'objet du cours. On va donc le désactiver sinon il va nous bloquer.

Dans [le mémo install VM](../../cours/memo/install_vm.md) (touuuut en bas) je donne les instructions pour le désactiver (on l'avait fait quand on avait install une VM ensemble la première fois).
