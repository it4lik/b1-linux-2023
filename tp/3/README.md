# TP3 : Services

**Dans ce TP, on va jouer avec des *services*.**

Un *service* c'est juste un programme que l'OS lance et dont il s'occupe ensuite. Plutôt que de taper une commande pour lancer un programme, c'est l'OS qui lance la commande pour nous !

Ca a plein plein de bénéfices de faire ça, notamment :

- **programmer un démarrage automatique d'un programme** 
  - parce que ce serait plutôt chiant s'il fallait démarrer à la main les ~50+ programmes qui constituent votre OS
  - genre tu appuies sur le bouton pour démarrer ton PC et tu veux pas démarrer Windows à la main
- **programmer un REdémarrage automatique**
  - genre si un machin crash, PAP, il est relancé automatiquement
  - ça vous est déjà touuuus arrivé j'en suis sûr, d'avoir par exemple l'interface graphique qui crashe et redémarre
- **et plein d'autres bails qu'on verra plus tard...**

---

L'OS, comme on l'a dit c'est un ensemble de programmes, et donc pour être plus spécifiques, ils sont gérés comme des *services*.

On peut aussi sans problème ajouter des nouveaux *services* à une machine ou en créer nous-mêmes, et c'est bien sûr l'objectif du TP n_n

On va donc bosser sur des trucs un peu cas d'école :

- un *service* déjà installé sur nos VMs : le serveur SSH
- un *service* qu'on va ajouter : un serveur Web
- un *service* qu'on va créer : un ptit chat avec `netcat`

On va donc ici se pencher sur trois *services* qu'on pourrait appeler des *services* réseau. En effet, ces trois programmes écoutent sur un port afin d'accueillir des clients.

> Sur votre PC il existe aussi tout un tas de *services* : il y en a un qui gère le WiFi, un autre le bluetooth, un autre les disques durs, un autre l'heure de votre PC, etc. Ce concept de "*service*" est commun à tous les OS.

Ce que vous faites dans ce TP deviendra peu à peu naturel au fil des cours et de votre utilsation de GNU/Linux.

Comme d'hab rien à savoir par coeur, jouez le jeu, et la plasticité de votre cerveau fera le reste.

# 0. Setup

➜ **Que du Rocky Linux pour tout le monde**

- l'interface graphique c'était pour vous aider à la transition aux TPs précédents
- on a fait cela dit tous les TPs en terminal, donc vous commencez à saisir qu'on en a pas besoin de l'interface graphique si on a pas besoin de Photoshop ou League of Legends :)
- sur un serveur, on considère que l'interface graphique alourdit inutilement la charge de la machine, donc souvent, y'en a juste pas
- on utilise donc SSH pour prendre le contrôle de la machine à distance !s

➜ **Une seule machine Rocky Linux suffit pour tout le TP**

- assurez-vous de dérouler la [checklist](#checklist) sur la machine avant de continuer
- donnez-lui l'adresse IP `10.3.1.11/24` et le hostname `node1.tp3.b1`

➜ **Munissez-vous du [mémo de commandes Linux](../../cours/memo/shell.md)**

- dans ce TP on va taper plein de commandes pour :
  - créer/modifier des fichiers/dossiers
  - interagir avec des services
  - installer des paquets
  - etc.

# Suite du TP

- [La checklist, que vous déroulerez à chaque fois que vous créerez une VM](./checklist.md)
  - genre ce sera la même à tous les TPs
  - rien à rendre, c'est juste pour vous assurer que vous avez une VM ready to go
- [Partie 1 : service SSH](./ssh.md)
- [Partie 2 : service Web](./web.md)
- [Partie 3 : service netcat fait maison](./netcat.md)
