# TP 5 : We do a little scripting

Aujourd'hui un TP pour appréhender un peu **le scripting `bash`**.

➜ **Le scripting dans GNU/Linux**, c'est simplement le fait d'écrire dans un fichier une suite de commande, qui seront exécutées les unes à la suite des autres lorsque l'on exécutera le script.

Plus précisément, on utilisera la syntaxe du shell `bash`. Et on a le droit à l'algo (des variables, des conditions `if`, des boucles `while`, etc).

➜ **`bash` c'est un langage de scripting OS**, donc il a une place particulière parmi tous les langages

C'est pas DU TOUT adapté pour dév un site web par exemple. Nul. Jeux-vidéos étou on oublie.

Par contre c'est parfait et très direct pour manipuler tout ce qui est lié à l'OS : créer/éditer des fichiers, gérer des users, des services, etc. Ou automatiser le déploiement de 1000 serveurs Web. Ou 1000 serveurs minecraft. Ou déployer la planète, t'as capté.

Bah encore une fois, le concept c'est juste d'enchaîner des commandes qu'on aurait pu taper à la main dans le terminal, donc ce qui est faisable depuis le terminal, on peut l'automatiser en faisant un script.

> *Sur Windows on utiliserait Powershell comme langage de scripting.*

➜ **C'est bon à prendre pour tout le monde**

Je t'assure que tu taperas des commandes `bash` peu importe ton métier dans l'informatique, du moment que tu restes technicien. Les OS Linux sont partout tellement c'est un OS customisable.

Que ce soit pour compiler ton jeu, déployer ton site web, ou n'importe quoi d'autres, c'est pas qu'un truc de barbu linuxien de savoir aligner 3 lignes de `bash` sans trembler des genoux. C'est devenu un skill de base.

➜ **Une seule VM suffira pour ce TP**

- rien d'imposé, OS de votre choix
- je vous recommande le flow suivant :
  - code dans votre IDE préféré sur votre PC (VSCode ou autre)
  - envoi du code avec SCP sur la machine
  - exécution du code sur la VM

> Autrement dit, je ne vous recommande PAS de développer directement sur la machine, bien que ça me fasse toujours sourire de voir des gens dév avec `nano` hihi.

➜ **Bon par contre, la syntaxe `bash`, elle fait mal aux dents** quand on commence à faire de l'algo étou. Ca va prendre un peu de temps pour s'habituer.

![Bash syntax](./img/bash_syntax.jpg)

**Pour ça, vous prenez connaissance des ressources suivantes :**

- **[le cours sur le shell](../../cours/shell/README.md)**
- **[le cours sur le scripting](../../cours/scripting/README.md)**
- et [toujours le mémo de commandes](../../cours/memo/shell.md)

> Y'a aussi les très bons https://devhints.io/bash et https://learnxinyminutes.com/docs/bash/ pour tout ce qui est relatif à la syntaxe `bash`, c'est clair et bien résumé.

## Sommaire

- [TP 5 : We do a little scripting](#tp-5--we-do-a-little-scripting)
  - [Sommaire](#sommaire)
- [Echauffement](#echauffement)
- [La suiiiite](#la-suiiiite)

# Echauffement

Un premier script un peu useless, juste pour se familiariser avec la création d'un script.

➜ **Créer un fichier `test.sh` dans le dossier `/srv/` avec le contenu suivant** :

> Je vous recommande de coder avec votre IDE (VScode ou autre) sur votre machine, et d'envoyer vos scripts sur la VM avec `scp`.

```bash
#!/bin/bash
# Simple test script

echo "Connecté actuellement avec l'utilisateur $(whoami)."
```

> La première ligne est appelée le *shebang*. Cela indique le chemin du programme qui sera utilisé par le script. Ici on inscrit donc, pour un script `bash`, le chemin vers le programme `bash` mais on ferait la même chose pour des scripts en Python, PHP, etc.

➜ **Modifier les permissions du script `test.sh`**

- si c'est pas déjà le cas, faites en sorte qu'il appartienne à votre utilisateur avec la commande `chown`
- ajoutez la permission `x` pour votre utilisateur afin que vous puissiez exécuter le script avec la commande `chmod`

➜ **Exécuter le script** :

```bash
# Exécuter le script, peu importe le dossier où vous vous trouvez
$ /srv/test.sh

# Exécuter le script, depuis le dossier où il est stocké
$ cd /srv
$ ./test.sh

# pour que ça fonctionne, n'oubliez pas de rendre le script exécutable
$ chmod +x /srv/test.sh
```

> **Vos scripts devront toujours se présenter comme ça** : muni d'un *shebang* à la ligne 1 du script, appartenir à un utilisateur spécifique qui possède le droit d'exécution sur le fichier.

# La suiiiite

La suite du TP dans des documents séparés :

- [Partie 1 : Script idcard](./idcard.md)
- [Partie 2 : Téléchargements youtube](./youtubedl.md)
- [Partie 3 bonus : Automatisation du dernier TP](./automation.md)
