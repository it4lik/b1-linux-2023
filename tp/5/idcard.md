
# Partie 1 : Script carte d'identité

- [Partie 1 : Script carte d'identité](#partie-1--script-carte-didentité)
  - [Rendu](#rendu)
  - [Bonus](#bonus)

Vous allez écrire **un script qui récolte des informations sur le système et les affiche à l'utilisateur.** Il s'appellera `idcard.sh` et sera stocké dans `/srv/idcard/idcard.sh`.

> `.sh` est l'extension qu'on donne par convention aux scripts réalisés pour être exécutés avec `sh` ou `bash`.

➜ **L'emoji 🐚** est une aide qui indique une commande qui est capable de réaliser le point demandé

➜ **Testez les commandes à la main avant de les incorporer au script.**

➜ Ce que doit faire le script. Il doit afficher :

- le **nom de la machine**
  - 🐚 `hostnamectl`
- le **nom de l'OS** de la machine
  - regardez le fichier `/etc/redhat-release` ou `/etc/os-release`
  - 🐚 `source`
- la **version du noyau** Linux utilisé par la machine
  - 🐚 `uname`
- l'**adresse IP** de la machine
  - 🐚 `ip`
- l'**état de la RAM**
  - 🐚 `free`
  - espace dispo en RAM (en Go, Mo, ou Ko)
  - taille totale de la RAM (en Go, Mo, ou Ko)
- l'**espace restant sur le disque dur**, en Go (ou Mo, ou Ko)
  - donnez moi l'espace restant sur la partition `/`
  - 🐚 `df`
- le **top 5 des processus** qui pompent le plus de RAM sur la machine actuellement. Procédez par étape :
  - 🐚 `ps`
  - listez les process
  - affichez uniquement le nom du processus (voir exemple ci-dessous)
  - triez par RAM utilisée
  - isolez les 5 premiers
- la **liste des ports en écoute** sur la machine, avec le programme qui est derrière
  - préciser, en plus du numéro, s'il s'agit d'un port TCP ou UDP
  - 🐚 `ss`
  - je vous recommande d'utiliser une [syntaxe `while read`](../../cours/scripting/README.md#8-itérer-proprement-sur-plusieurs-lignes)
- la liste des dossiers disponibles dans la variable `$PATH`
- un **lien vers une image/gif** random de chat
  - 🐚 `curl`
  - il y a de très bons sites pour ça hihi
  - avec [celui-ci](https://thecatapi.com/) par exemple, une simple requête HTTP vous retourne l'URL d'une random image de chat
    - une requête sur cette adresse retourne une URL vers une photo random de chat
      - affichez juste l'URL à l'exécution du script (voir ci-dessous pour un exemple d'exécution)

Pour vous faire manipuler les sorties/entrées de commandes, votre script devra sortir **EXACTEMENT** :

```
$ /srv/idcard/idcard.sh
Machine name : ...
OS ... and kernel version is ...
IP : ...
RAM : ... memory available on ... total memory
Disk : ... space left
Top 5 processes by RAM usage :
  - python3
  - NetworkManager
  - systemd
  - ...
  - ...
Listening ports :
  - 22 tcp : sshd
  - ...
  - ...
PATH directories :
  - /usr/local/bin
  - ...
  - ...

Here is your random cat (jpg file) : https://....
```

> ⚠️ **Votre script doit fonctionner peu importe les conditions** : peu importe le nom de la machine, ou son adresse IP (genre il est interdit de récupérer pile 10 char sous prétexte que ton adresse IP c'est `10.10.10.1` et qu'elle fait 10 char de long)

## Rendu

📁 **Fichier `/srv/idcard/idcard.sh`**

🌞 **Vous fournirez dans le compte-rendu Markdown**, en plus du fichier, **un exemple d'exécution avec une sortie**

- genre t'exécutes ton script et tu copie/colles ça dans le compte-rendu

## Bonus

⭐ **Bonus 1 : arguments**

- si vous avez utilisé le lien que j'ai filé pour les images de chats, on est limités à 10 requêtes avant de devoir créer un compte
- créer un compte, ça donne un token
- ajoutez la possibilité de passer le token en argument

C'est à dire :

```bash
$ /srv/idcard/idcard.sh live_gxXl7Z0PjPMFVDdIM77T2c6beemFCHy8KRimDi0BGwZVOIqwCHbksr2Bva8VWUUV 
[...]
Here is your random cat (jpg file) : ./cat.jpg
```

Si aucun token est précisé, il y en a un par défaut qui est utilisé et c'est affiché dans la sortie :

```bash
$ /srv/idcard/idcard.sh  
[...]
Here is your random cat (jpg file) : ./cat.jpg (using default token)
```

⭐ **Bonus 2 : votre script doit s'exécuter en `root`**

> Vous apprend à mieux gérer vos permissions : un script donné qui a un but donné, c'est une bonne pratique que de créer un utilisateur dédié. Ainsi, on limite les privilèges donnés au script pendant son exécution.

- si le script est lancé avec un autre user que `root`, il quitte
  - vous pouvez utiliser la commande 🐚 `exit` pour que le script s'arrête
  - c'est obligatoire pour que la commande `ss` fonctionne comme attendu

---

⭐ **Bonus 3 : l'image du chat est téléchargée sur la machine**

> *Nécessite d'avoir fait le bonus 2. Vous apprend à gérer un téléchargement HTTP et quelques tricks shell en + pour la gestion d'ID.*

- **dans le dossier `/srv/cats/`**
  - le script quitte si ce dossier n'existe pas
- **le dossier `/srv/cats/` doit appartenir à l'utilisateur `id`**
  - le script quitte si c'est pas le cas
- **le dossier `/srv/cats/` doit être *writable* pour l'utilisateur `id`**
  - sinon il pourrait pas créer de fichiers dedans...
  - le script quitte si c'est pas le cas
- **renommer l'image avec un id**
  - l'image est renommée `1.jpg` pour la première image, puis `2.jpg` pour la deuxième, etc.
  - à chaque fois qu'on appelle le script, faut donc déterminer quel numéro est le prochain
  - gérer les IDs vaquants
    - si on télécharge 3 photos, mais qu'on supprime `2.jpg`, la prochaine photo téléchargée devra s'appeler `2.jpg`
    - on récupère l'ID vaquant `2` qui avait été libéré

---

⭐ **Bonus 4 : afficher + d'infos**

> *Vous demande d'aller creuser un peu plus et au passage apprendre un peu plus sur la façon dont l'OS fonctionne. Petit bout par petit bout.*

En plus des infos que votre script `/srv/idcard/idcard.sh` affiche déjà, ajoutez les infos suivantes :

- nombre d'inodes dispos sur la partition montée sur `/` et nombre total possible
  - afficher sous la forme :

```bash
$ /srv/idcard/idcard.sh
[...]
inodes on / partition : xxx/yyy
# où `xxx` c'est le nombre dispo et `yyy` le nombre total dans votre affichage
[...]
```

- liste des fichiers qui sont en SUID `root` sur la machine
  - afficher sous la forme :

```bash
$ /srv/idcard/idcard.sh
[...]
SUID files :
  - /path/to/suid
  - ...
[...]
```

- vérifier que la connexion en SSH en root est désactivée
  - il faut regarder le contenu du fichier de conf du serveur SSH pour ça
  - y'a une ligne qui permet d'autoriser ou non la connexion en root
  - afficher sous la forme :

```bash
$ /srv/idcard/idcard.sh
[...]
SSH root connection enabled : yes/no
# affiche seulement yes ou seulement no chez vous, en fonction de si c'est activé ou non
[...]
```
