# Partie 3 bonus : Automatisation

![Automated ?](./img/automation.png)

**Le principe de ce bonus est simple : complètement automatiser la réalisation du [TP4 : Real services](../4/README.md).**

Dans ce TP4, on avait deux machines : `web` et `storage`. Ce bonus consiste donc à réaliser deux scripts : `web.sh` et `storage.sh`.

**L'idée est la suivante :**

- je crée deux machines Rocky Linux
- je fais juste la conf de base (genre la checklist que je vous file pour les TPs)
- sur l'une des deux, j'exécute `storage.sh`
- sur l'autre, j'exécute `web.sh`
- le TP4 est terminé

Autrement dit, ces deux scripts réalisent dans l'ordre les commandes pour arriver à l'état final demandé dans le TP4.

Faites ça clean :

- **output custom**
  - je veux pas avoir l'output de `dnf install` jeté au visage
  - ptit output custom stylé genre...

```bash
$ ./storage.sh
Start LVM partition setup...
LVM partition setup finished.
Installing NFS utils...
NFS utils installed.
...
Node web is ready !
```

- **le script doit être lancé en `root`**
  - bah oui script qui va installer des paquets étou...
  - s'il est pas lancé en `root` il quitte
